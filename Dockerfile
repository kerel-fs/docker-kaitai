FROM openjdk:8-stretch
MAINTAINER Vasilis Tsiligiannis <acinonyx@openwrt.gr>

WORKDIR /workdir/

ARG KAITAI_VERSION

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		apt-transport-https \
	&& apt-key adv --keyserver hkp://pool.sks-keyservers.net --recv 379CE192D401AB61 \
	&& echo "deb https://dl.bintray.com/kaitai-io/debian jessie main" > /etc/apt/sources.list.d/kaitai.list \
	&& apt-get update \
	&& apt-get install -y --no-install-recommends \
		kaitai-struct-compiler${KAITAI_VERSION:+=$KAITAI_VERSION} \
	&& rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/bin/ksc"]
